<?php
    require('getSchedule.php');
?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/ds19.css">
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/Schedule.js"></script>

    <title>Konferenz Wegweiser</title>
</head>
<body>
    <div class="container w-100 fullscreen">

        <div class="row">
            <div class="col">
                <h1>Konferenz Wegweiser</h1>
            </div>
            <div class="col">
                <h1>
                    <span class="conf_title">loading schedule..</span>
                </h1>
                Fahrplan-Version: <span class="conf_version"></span>
            </div>
        </div>

        <!--  PAGE 1  !-->

        <div class="page page1">
            <div class="row">
                <div class="col">
                    <h3>nächste Talks:</h3>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card-deck talks talks_plan"></div>
                </div>
            </div>
            <!--
            <div class="row">
                <div class="col-sm text-center mt-3">
                <img class="img-fluid h-75" alt="Grundriss" src="img/plan4.svg" />
                </div>
            </div>
            !-->
        </div>

        <!--  PAGE 2  !-->

        <div class="page page2">
            <div class="row mt-5">
                <div class="col-sm text-center align-middle mt-3">
                    <img class="img-fluid h-100" alt="Grundriss" src="img/plan3.svg" />
                </div>
            </div>
        </div>

        <!--  PAGE 3  !-->

        <div class="page page3">
        <div class="row">
            <div class="col">
                <h3>nächste Talks:</h3>
            </div>
        </div>
        <div class="card-columns talks talks_overview"></div>
    </div>


<script>
    setTimeout(function () {
        location.reload();
    }, 60000);

    function setPage(num)
    {
        $('.page').hide();
        $('.page'+num).fadeIn(500);
    }

    function event_view(event)
    {
        return `   <div class="card">
                        <div class="card-title">
                            <h2> ${event.title} </h2>
                        </div>
                        <div class="card-body">
                            <p> ${event.abstract} </p>
                        </div>
                        <div class="card-footer">
                            <h2>${(event.persons !== "\n") ? event.persons : "<br>"}</h2>
                            <h2 class="col text-right roomname">${event.room}</h2>
                            <p>
                                <h2 class="float-left ml-1">${event.start}Uhr</h2>
                                <h2 class="float-right mr-2">${event.duration}h</h2>
                            </p>
                        </div>
                    </div>`;
    }

    function event_view_small(event, noAbstract = false)
    {
        if (noAbstract === true){
            event.abstract = '';
        }

        return `   <div class="card">
                        <div class="card-title ml-1">
                            <h4 class="font-weight-bold"> ${event.title} </h4>
                        </div>
                        <div class="card-title font-italic text-right mr-1">
                            <h4>${(event.persons !== "\n") ? event.persons : "<br>"}</h4>
                        </div>
                        <div class="card-footer">
                            <div class="col text-center roomname">
                                <div class="row">
                                    <div class="col text-left">
                                        <h4 class="">${event.start}Uhr</h4>
                                    </div>
                                    <div class="col">
                                        <h4 class="roomname">${event.room}</h4>
                                    </div>
                                    <div class="col text-right">
                                        <h4>${event.duration}h</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;
    }

    $.when($.ready).then(function () {
        let path = "./schedule/schedule.xml";
        $.get(path, function (data) {
            this.xml = new XMLSerializer().serializeToString(data);

            // load your schedule
            var scheduleXml = $($.parseXML(this.xml));
            var s = new Schedule(scheduleXml);

            // for testing purposes
            // Example: ?faketime=2018-09-22T19:29:00
            let faketime = new URL(window.location.href).searchParams.get("faketime");
            if (faketime != null) {
                s.setTimeTo(new Date(faketime));
            }
            s.getEvents();

            // output part of the schedule
            $('.conf_version').html(s.getScheduleVersion());
            $('.conf_title').html(s.getScheduleTitle());
            $('.conf_start').html(s.getScheduleStart());
            $('.conf_end').html(s.getScheduleEnd());

            // load events
            var events = s.getNextEvents(3);
            events.forEach(event => {
                $('.talks_plan').append(event_view(event));
            });
            var eventObj = s.getEventsAllTracks(10);
            eventObj.events.forEach(event => {
                $('.talks_overview').append(event_view_small(event, true));
            });

            // change screen after 20Sek and 40Sek
            setTimeout(function () {
                setPage(1);
            }, 1);

            setTimeout(function () {
                setPage(2);
            }, 20000);

            setTimeout(function () {
                setPage(3);
            }, 40000);

        });

    })
</script>
</body>
</html>
