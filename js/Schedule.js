/**
 *  get the data from schedule
 */
class Schedule {

    /**
     * class constructor
     */
    constructor(scheduleXml){
        /**
         *  the schedule xml object parsed as jQuery object
         */
        this.schedule = false;

        /**
         * Array of Events
         *
         * [{
         *  id: 2132,
         *  title: "foobar"
         *  date:2018-09-22T10:30:00+02:00
         *   }]
         */
        this.events = [];

        /**
         *  object with Information of the conference
         *  {'title', 'start', 'end', ..}
         */
        this.conf = {
            title:  'defaultConf',
            start:  '1984-09-23',
            end:    '1984-09-24'
        }

        /**
         * indepent time
         */
        this.now = new Date();

        this.schedule = scheduleXml.children("schedule");

        this.conf.version   = this.schedule.children("version").text();
        this.conf.title     = this.schedule.children("conference").children("title").text();
        this.conf.start     = this.schedule.children("conference").children("start").text();
        this.conf.end       = this.schedule.children("conference").children("end").text();
    }

    /**
     * set orientation time
     */
    setTimeTo(time){
        this.now = time;
    }

    /**
     * @return String the schedule version
     */
    getScheduleVersion(){
        return this.conf.version    ;
    }
    /**
     * @return String the schedule title
     */
    getScheduleTitle(){
        return this.conf.title;
    }
    /**
     * @return day of conf start
     */
    getScheduleStart(){
        return this.conf.start;
    }
    /**this.events
     * @return day of conf end
     */
    getScheduleEnd(){
        return this.conf.end;
    }

    /**
     * return array of all Events
     */
    getEvents()
    {
        let allEvents = [];
        //this.schedule.find('event').toArray();
        var now = this.now
        this.schedule.find('event').each( function(){

            var eventStart = new Date($(this).children('date').text());
            var tenHours = 36000000;
            if ((eventStart - now) >= 0 && (eventStart - now) < tenHours){
                allEvents.push({
                    title:      $(this).children('title').text(),
                    room:       $(this).children('room').text(),
                    date:       $(this).children('date').text(),
                    start:      $(this).children('start').text(),
                    duration:   $(this).children('duration').text(),
                    abstract:   $(this).children('abstract').text().slice(0, 256),
                    persons:    $(this).children('persons').text(),
                });
            }
        });

        allEvents.sort((a,b) => {

            var a_start = new Date(a.date).getTime();
            var b_start = new Date(b.date).getTime();
            console.log(a_start, b_start, a_start - b_start);
            return a_start - b_start;
        });

        this.events = allEvents;
    }

    getEventsAllTracks(count)
    {
        let roomlist = [];
        let events = this.getNextEvents(count);
        events.forEach( event => {
            if (0 == roomlist.filter(item => event.room == item).length){
                roomlist.push(event.room);
            }
        });
        return {
            events:     events,
            roomlist:   roomlist
        }
    }

    /**
     * get next Events by time and room
     *
     * @param {Timestamp} minimum time to start
     * @param {*} room
     */
    getTalk(time, room){
        return talk;
    }

    /**
     * get the next Events, based on time
     *
     */
    getNextEvents(count){
        if (this.events.length == 0){
            return this.events.slice(0, count);
        }

        let talks = [];

        let now = this.now;

        this.events.forEach( talk => {
            let talkStart = new Date(talk.date).getTime();
            if ( talkStart >= now ){
                talks.push(talk);
            }
        });

        return this.events.slice(0, count);
    }

}
